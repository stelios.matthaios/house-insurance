# House Insurance App

## Notes
- The exercise was implemented using Spring Boot to bootstrap the project. 
- Spock framework was used for unit testing.
- The 3rd party libraries were not implemented or covered by unit tests as they are considered out of scope
- No functional tests were added for a couple of reasons. Keep the scope small (fit in 2 hours). The functionality was not completed end to end as the 3rd party libraries were missing.


## Implementation break down 
- The house was modeled as an object for future use.
- A profile was added (uk) in order to be able to extend the functionality and support more countries. Some classes/beans that were considered country specific (service and validator) were added in the profile.
- Properties were used for values that could be changed in the future. 
- Adapters were used for the 3rd parties to keep our domain clean. The main responsibility of the adapters was to translate the 3rd party errors and objects.
- JaCoCo has 80% minimum line and branch coverage
- Multiple errors were added (checked and unchecked depending on the case) even though all were handled the same way, for future use. 

## Future thoughts
- Some plugins should be added like checkstyle, pmd and findbugs to help developers extend the code. 
- Functional tests should be added after mocking the 3rd party implementation.
