package gr.hellasdirect.houseinsurance.adapter;

import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException;
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import uk.co.subsidencewatch.SubsidenceAreaChecker;
import uk.co.subsidencewatch.SubsidenceAreaCheckerTechnicalFailureException;

@Service
@AllArgsConstructor
public class SubsidenceAreaCheckerAdapter {
    private SubsidenceAreaChecker subsidenceAreaChecker;

    public void checkSubsidenceArea(String postcode) throws HouseInsuranceCannotBeInsuredException {
        try {
            if (subsidenceAreaChecker.isPostcodeInSubsidenceArea(postcode)) {
                throw new HouseInsuranceCannotBeInsuredException(
                        String.format(
                                "Postcode %s is in subsidence area",
                                postcode
                        )
                );
            }
        } catch (SubsidenceAreaCheckerTechnicalFailureException e) {
            throw new HouseInsuranceException(String.format("Failed to retrieve response from subsidenceAreaChecker" +
                    "for postcode %s", postcode), e.getCause());
        }
    }
}
