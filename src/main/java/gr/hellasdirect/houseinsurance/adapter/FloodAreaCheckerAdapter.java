package gr.hellasdirect.houseinsurance.adapter;

import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties;
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException;
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceException;
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import uk.co.floodwatch.FloodAreaChecker;
import uk.co.floodwatch.FloodAreaCheckerTechnicalFailureException;
import uk.co.floodwatch.FloodRisk;
import uk.co.floodwatch.PostcodeNotFoundException;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class FloodAreaCheckerAdapter {
    private FloodAreaChecker floodAreaChecker;
    private HouseInsuranceProperties floodProperties;

    public BigDecimal getFloodAreaRisk(String postcode) throws HouseInsurancePostcodeNotFoundException,
            HouseInsuranceCannotBeInsuredException {
        try {
            FloodRisk risk = floodAreaChecker.isPostcodeInFloodArea(postcode);

            switch(risk) {
                case HIGH_RISK:
                    throw new HouseInsuranceCannotBeInsuredException();
                case MEDIUM_RISK:
                    return floodProperties.getFloodChargeMediumPercentage();
                default:
                    return floodProperties.getFloodChargeLowPercentage();
            }
        } catch (FloodAreaCheckerTechnicalFailureException tfe) {
            throw new HouseInsuranceException(
                String.format(
                        "Postcode %s could not be checked for flood area.",
                        postcode
                )
            );
        } catch (PostcodeNotFoundException nfe) {
            throw new HouseInsurancePostcodeNotFoundException(
                String.format(
                        "Postcode %s was not found when checked for flood.",
                        postcode
                )
            );
        }
    }
}
