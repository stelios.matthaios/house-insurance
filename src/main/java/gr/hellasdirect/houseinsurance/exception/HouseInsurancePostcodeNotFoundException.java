package gr.hellasdirect.houseinsurance.exception;

public class HouseInsurancePostcodeNotFoundException extends Exception {

    public HouseInsurancePostcodeNotFoundException() {
    }

    public HouseInsurancePostcodeNotFoundException(String message) {
        super(message);
    }

    public HouseInsurancePostcodeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
