package gr.hellasdirect.houseinsurance.exception;

public class HouseInsuranceCannotBeInsuredException extends Exception {

    public HouseInsuranceCannotBeInsuredException() {
    }

    public HouseInsuranceCannotBeInsuredException(String message) {
        super(message);
    }

    public HouseInsuranceCannotBeInsuredException(String message, Throwable cause) {
        super(message, cause);
    }
}
