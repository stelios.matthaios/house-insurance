package gr.hellasdirect.houseinsurance.exception;

public class HouseInsuranceException extends RuntimeException {

    public HouseInsuranceException() {
    }

    public HouseInsuranceException(String message) {
        super(message);
    }

    public HouseInsuranceException(String message, Throwable cause) {
        super(message, cause);
    }
}
