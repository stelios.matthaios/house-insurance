package gr.hellasdirect.houseinsurance.service;

import gr.hellasdirect.houseinsurance.adapter.FloodAreaCheckerAdapter;
import gr.hellasdirect.houseinsurance.adapter.SubsidenceAreaCheckerAdapter;
import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties;
import gr.hellasdirect.houseinsurance.domain.House;
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException;
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceException;
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException;
import gr.hellasdirect.houseinsurance.validator.HouseInsuranceValidator;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Profile("uk")
@AllArgsConstructor
public class HouseInsuranceServiceUKImpl implements HouseInsuranceService {
    private FloodAreaCheckerAdapter floodAreaChecker;
    private HouseInsuranceValidator houseInsuranceValidator;
    private HouseInsuranceProperties properties;
    private SubsidenceAreaCheckerAdapter subsidenceAreaChecker;

    @Override
    public BigDecimal calculatedPremium(String postcode, int numBedrooms, boolean hasThatchedRoof)
            throws HouseInsurancePostcodeNotFoundException, HouseInsuranceCannotBeInsuredException {
        houseInsuranceValidator.validate(postcode, numBedrooms, hasThatchedRoof);

        BigDecimal basicPremium = properties.getBasicPremium();
        House house = new House(postcode, numBedrooms, hasThatchedRoof);

        BigDecimal floodRiskMultiplier = floodAreaChecker.getFloodAreaRisk(house.getPostcode());
        subsidenceAreaChecker.checkSubsidenceArea(house.getPostcode());

        return floodRiskMultiplier.multiply(basicPremium);
    }

    @Override
    public String calculatedPremiumText(String postcode, int numBedrooms, boolean hasThatchedRoof) {
        try {
            BigDecimal premium = calculatedPremium(postcode, numBedrooms, hasThatchedRoof);

            return String.format(
                    "Premium %s %s",
                    premium.setScale(2, RoundingMode.CEILING),
                    properties.getCurrency()
            );
        } catch (HouseInsuranceCannotBeInsuredException | HouseInsurancePostcodeNotFoundException ex) {
            return "The insurance is not available";
        }
    }
}
