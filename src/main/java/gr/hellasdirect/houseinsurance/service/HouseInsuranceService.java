package gr.hellasdirect.houseinsurance.service;

import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException;
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException;

import java.math.BigDecimal;

public interface HouseInsuranceService {
  BigDecimal calculatedPremium(String postcode, int numBedrooms, boolean hasThatchedRoof) throws HouseInsurancePostcodeNotFoundException, HouseInsuranceCannotBeInsuredException;
  String calculatedPremiumText(String postcode, int numBedrooms, boolean hasThatchedRoof);
}
