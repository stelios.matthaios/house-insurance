package gr.hellasdirect.houseinsurance.validator;

import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException;
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException;

public interface HouseInsuranceValidator {
    void validate(String postcode, int numberOfBedrooms, boolean isHatched)
            throws HouseInsuranceCannotBeInsuredException, HouseInsurancePostcodeNotFoundException;
}
