package gr.hellasdirect.houseinsurance.validator;

import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties;
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException;
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
@Component
@Profile("uk")
public class HouseInsuranceValidatorImpl implements HouseInsuranceValidator {
    private HouseInsuranceProperties properties;

    public void validate(String postcode, int numberOfBedrooms, boolean isThatched)
            throws HouseInsuranceCannotBeInsuredException, HouseInsurancePostcodeNotFoundException {
        Pattern pattern = Pattern.compile(properties.getPostcodeRegex());
        Matcher matcher = pattern.matcher(postcode);

        if (!matcher.matches()) {
            throw new HouseInsurancePostcodeNotFoundException("Invalid postcode pattern");
        }
        if (isThatched) {
            throw new HouseInsuranceCannotBeInsuredException("The roof is thatched");
        }
        if (numberOfBedrooms < 0) {
            throw new HouseInsuranceCannotBeInsuredException("Invalid number of bedrooms");
        }
        if (numberOfBedrooms > properties.getMaximumBedrooms()) {
            throw new HouseInsuranceCannotBeInsuredException("Number of bedrooms exceeded supported limit");
        }
    }
}
