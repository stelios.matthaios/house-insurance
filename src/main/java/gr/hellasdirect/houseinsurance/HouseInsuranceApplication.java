package gr.hellasdirect.houseinsurance;

import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(HouseInsuranceProperties.class)
public class HouseInsuranceApplication {
    public static void main(String[] args) {
        SpringApplication.run(HouseInsuranceApplication.class, args);
    }
}
