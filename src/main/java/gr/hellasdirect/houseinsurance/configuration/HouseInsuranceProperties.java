package gr.hellasdirect.houseinsurance.configuration;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.math.BigDecimal;

@Getter
@ConfigurationProperties("insurance")
public class HouseInsuranceProperties {
    private BigDecimal basicPremium;
    private String currency;
    private BigDecimal floodChargeMediumPercentage;
    private BigDecimal floodChargeLowPercentage;
    private int maximumBedrooms;
    private String postcodeRegex;
}
