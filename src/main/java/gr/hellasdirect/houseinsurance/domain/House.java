package gr.hellasdirect.houseinsurance.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Getter
@Validated
@AllArgsConstructor
public class House {
    private String postcode;

    @Min(0)
    private int numberOfBedrooms;

    private boolean thatchedRood;
}
