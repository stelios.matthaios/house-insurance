package gr.hellasdirect.houseinsurance.service

import gr.hellasdirect.houseinsurance.adapter.FloodAreaCheckerAdapter
import gr.hellasdirect.houseinsurance.adapter.SubsidenceAreaCheckerAdapter
import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException
import gr.hellasdirect.houseinsurance.validator.HouseInsuranceValidator
import spock.lang.Specification

class HouseInsuranceServiceUKImplSpec extends Specification {
    private static final String POSTCODE = 'POSTCODE'
    private static final BigDecimal BASIC_MEDIUM = BigDecimal.TEN
    private static final String ERROR_TEXT = 'The insurance is not available'
    private static final BigDecimal FLOOD_MULTIPLIER = BigDecimal.ONE
    private static final int NUMBER_OF_BEDROOMS = 2
    private static final boolean IS_THATCHED_ROOF = false
    private static final String SUCCESS_TEXT = 'Premium 10.00 pounds'
    private static final String UK_CURRENCY = 'pounds'

    private HouseInsuranceService houseInsuranceService

    private FloodAreaCheckerAdapter floodAreaChecker
    private HouseInsuranceValidator houseInsuranceValidator
    private HouseInsuranceProperties properties
    private SubsidenceAreaCheckerAdapter subsidenceAreaChecker

    def setup() {
        floodAreaChecker = Mock()
        houseInsuranceValidator = Mock()
        properties = Mock(HouseInsuranceProperties) {
            getCurrency() >> UK_CURRENCY
        }
        subsidenceAreaChecker = Mock()

        houseInsuranceService = new HouseInsuranceServiceUKImpl(floodAreaChecker, houseInsuranceValidator, properties, subsidenceAreaChecker)
    }

    def 'Calculate premium - Validation error'() {
        given: 'An error'
            HouseInsurancePostcodeNotFoundException actual = new HouseInsurancePostcodeNotFoundException()

        when: 'The premium is calculated'
            houseInsuranceService.calculatedPremium(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input and throws an error'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF) >> {
                throw actual
            }
        and: 'The exceptions is propagated'
            HouseInsurancePostcodeNotFoundException expected = thrown()
            expected == actual
        and: 'No other interactions happen'
            0 * _
    }

    def 'Calculate premium - Flood checker error'() {
        given: 'An error'
            HouseInsurancePostcodeNotFoundException actual = new HouseInsurancePostcodeNotFoundException()

        when: 'The premium is calculated'
            houseInsuranceService.calculatedPremium(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)
        and: 'The basic premium is retrieved'
            1 * properties.getBasicPremium() >> BASIC_MEDIUM
        and: 'The flood area checker returns an error for this postcode'
            1 * floodAreaChecker.getFloodAreaRisk(POSTCODE) >> {
                throw actual
            }
        and: 'The exceptions is propagated'
            HouseInsurancePostcodeNotFoundException expected = thrown()
            expected == actual
        and: 'No other interactions happen'
            0 * _
    }

    def 'Calculate premium - Subsidence area checker error'() {
        given: 'An error'
            HouseInsurancePostcodeNotFoundException actual = new HouseInsurancePostcodeNotFoundException()

        when: 'The premium is calculated'
            houseInsuranceService.calculatedPremium(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)
        and: 'The basic premium is retrieved'
            1 * properties.getBasicPremium() >> BASIC_MEDIUM
        and: 'The flood area risk is retrieved'
            1 * floodAreaChecker.getFloodAreaRisk(POSTCODE)
        and: 'The subsidence area checker returns an error for this postcode'
            1 * subsidenceAreaChecker.checkSubsidenceArea(POSTCODE) >> {
                throw actual
            }
        and: 'The exceptions is propagated'
            HouseInsurancePostcodeNotFoundException expected = thrown()
            expected == actual
        and: 'No other interactions happen'
            0 * _
    }

    def 'Calculate premium'() {
        when: 'The premium is calculated'
            def result = houseInsuranceService.calculatedPremium(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)
        and: 'The basic premium is retrieved'
            1 * properties.getBasicPremium() >> BASIC_MEDIUM
        and: 'The flood area risk is retrieved'
            1 * floodAreaChecker.getFloodAreaRisk(POSTCODE) >> FLOOD_MULTIPLIER
        and: 'The subsidence area check is done'
            1 * subsidenceAreaChecker.checkSubsidenceArea(POSTCODE)
        and: 'The result is the expected'
            result == BASIC_MEDIUM * FLOOD_MULTIPLIER
    }

    def 'Calculate premium text - Validation error'() {
        given: 'An error'
            HouseInsuranceCannotBeInsuredException actual = new HouseInsuranceCannotBeInsuredException()

        when: 'The premium text is calculated'
            def result = houseInsuranceService.calculatedPremiumText(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input and throws an error'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF) >> {
                throw actual
            }
        and: 'The result is the expected'
            result == ERROR_TEXT
    }

    def 'Calculate premium text - Flood checker error'() {
        given: 'An error'
            HouseInsurancePostcodeNotFoundException actual = new HouseInsurancePostcodeNotFoundException()

        when: 'The premium text is calculated'
            def result = houseInsuranceService.calculatedPremiumText(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)
        and: 'The basic premium is retrieved'
            1 * properties.getBasicPremium() >> BASIC_MEDIUM
        and: 'The flood area checker returns an error for this postcode'
            1 * floodAreaChecker.getFloodAreaRisk(POSTCODE) >> {
                throw actual
            }
        and: 'The result is the expected'
            result == ERROR_TEXT
    }

    def 'Calculate premium text - Subsidence area checker error'() {
        given: 'An error'
            HouseInsurancePostcodeNotFoundException actual = new HouseInsurancePostcodeNotFoundException()

        when: 'The premium text is calculated'
            def result = houseInsuranceService.calculatedPremiumText(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)
        and: 'The basic premium is retrieved'
            1 * properties.getBasicPremium() >> BASIC_MEDIUM
        and: 'The flood area risk is retrieved'
            1 * floodAreaChecker.getFloodAreaRisk(POSTCODE)
        and: 'The subsidence area checker returns an error for this postcode'
            1 * subsidenceAreaChecker.checkSubsidenceArea(POSTCODE) >> {
                throw actual
            }
        and: 'The result is the expected'
            result == ERROR_TEXT
    }

    def 'Calculate premium text'() {
        when: 'The premium text is calculated'
            def result = houseInsuranceService.calculatedPremiumText(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)

        then: 'The validator validates the input'
            1 * houseInsuranceValidator.validate(POSTCODE, NUMBER_OF_BEDROOMS, IS_THATCHED_ROOF)
        and: 'The basic premium is retrieved'
            1 * properties.getBasicPremium() >> BASIC_MEDIUM
        and: 'The flood area risk is retrieved'
            1 * floodAreaChecker.getFloodAreaRisk(POSTCODE) >> FLOOD_MULTIPLIER
        and: 'The subsidence area check is done'
            1 * subsidenceAreaChecker.checkSubsidenceArea(POSTCODE)
        and: 'The result is the expected'
            result == SUCCESS_TEXT
    }
}
