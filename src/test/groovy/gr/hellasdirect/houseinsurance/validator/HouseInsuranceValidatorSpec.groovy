package gr.hellasdirect.houseinsurance.validator

import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException
import spock.lang.Specification

class HouseInsuranceValidatorSpec extends Specification {
    private static final int MAXIMUM_NUMBER_OF_BEDROOMS = 5
    private static final String INVALID_POSTCODE = '123 0RT'
    private static final String POSTCODE_REGEX = '^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$'
    private static final String VALID_POSTCODE = 'RE1W 0RT'

    private HouseInsuranceValidator validator
    private HouseInsuranceProperties properties

    def setup() {
        properties = Mock(HouseInsuranceProperties) {
            getMaximumBedrooms() >> MAXIMUM_NUMBER_OF_BEDROOMS
            getPostcodeRegex() >> POSTCODE_REGEX
        }

        validator = new HouseInsuranceValidatorImpl(properties)
    }

    def 'Invalid input parameters for #description and valid postcode'() {
        when: 'The input is validated for #description'
            validator.validate(VALID_POSTCODE, numberOfBedrooms, isHatched)

        then: 'An exception is thrown'
             thrown(HouseInsuranceCannotBeInsuredException)

        where:
        description                                                   |   numberOfBedrooms                |       isHatched
        'invalid number of bedrooms with hatched roof'                |   -1                              |       true
        'invalid number of bedrooms without hatched roof'             |   -1                              |       false
        'more than maximum number of bedrooms with hatched roof'      |   MAXIMUM_NUMBER_OF_BEDROOMS + 1  |       true
        'more than maximum number of bedrooms without hatched roof'   |   MAXIMUM_NUMBER_OF_BEDROOMS + 1  |       false
        'valid number of bedrooms with hatched roof'                  |   MAXIMUM_NUMBER_OF_BEDROOMS - 1  |       true
    }

    def 'Invalid postcode'() {
        when: 'The input is validated'
            validator.validate(INVALID_POSTCODE, MAXIMUM_NUMBER_OF_BEDROOMS - 1, false)

        then: 'An exception is thrown'
             thrown(HouseInsurancePostcodeNotFoundException)
    }

    def 'Valid input parameters'() {
        when: 'The input is validated for #description'
            validator.validate(VALID_POSTCODE, MAXIMUM_NUMBER_OF_BEDROOMS - 1, false)

        then: 'No exception is thrown'
             noExceptionThrown()
    }
}
