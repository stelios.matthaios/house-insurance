package gr.hellasdirect.houseinsurance.adapter

import gr.hellasdirect.houseinsurance.configuration.HouseInsuranceProperties
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceException
import gr.hellasdirect.houseinsurance.exception.HouseInsurancePostcodeNotFoundException
import spock.lang.Specification
import uk.co.floodwatch.FloodAreaChecker
import uk.co.floodwatch.FloodAreaCheckerTechnicalFailureException
import uk.co.floodwatch.FloodRisk
import uk.co.floodwatch.PostcodeNotFoundException

class FloodAreaCheckerAdapterSpec extends Specification {
    private static final String POSTCODE = 'POSTCODE'
    private static final BigDecimal FLOOD_CHARGE_MEDIUM_PERCENTAGE = new BigDecimal(2)
    private static final BigDecimal FLOOD_CHARGE_LOW_PERCENTAGE = BigDecimal.ONE

    private FloodAreaCheckerAdapter floodAreaCheckerAdapter

    private FloodAreaChecker floodAreaChecker
    private HouseInsuranceProperties properties

    def setup() {
        floodAreaChecker = Mock()
        properties = Mock(HouseInsuranceProperties) {
            getFloodChargeMediumPercentage() >> FLOOD_CHARGE_MEDIUM_PERCENTAGE
            getFloodChargeLowPercentage() >> FLOOD_CHARGE_LOW_PERCENTAGE
        }

        floodAreaCheckerAdapter = new FloodAreaCheckerAdapter(floodAreaChecker, properties)
    }

    def 'Flood area checker - technical failure'() {
        when: 'The flood risk for the area of the postcode is checked'
            floodAreaCheckerAdapter.getFloodAreaRisk(POSTCODE)

        then: 'The 3rd party for a given postcode is called and a technical error is thrown'
            1 * floodAreaChecker.isPostcodeInFloodArea(POSTCODE) >> {
                throw new FloodAreaCheckerTechnicalFailureException()
            }
        and: 'The correct error is thrown'
            thrown(HouseInsuranceException)
    }

    def 'Flood area checker - post code not found'() {
        when: 'The flood risk for the area of the postcode is checked'
            floodAreaCheckerAdapter.getFloodAreaRisk(POSTCODE)

        then: 'The 3rd party for a given postcode is called but the postcode was not found'
            1 * floodAreaChecker.isPostcodeInFloodArea(POSTCODE) >> {
                throw new PostcodeNotFoundException()
            }
        and: 'The correct error is thrown'
            thrown(HouseInsurancePostcodeNotFoundException)
    }

    def 'Flood area checker - high risk'() {
        when: 'The flood risk for the area of the postcode is checked'
            floodAreaCheckerAdapter.getFloodAreaRisk(POSTCODE)

        then: 'The 3rd party for a given postcode is called and indicates a high flood risk'
            1 * floodAreaChecker.isPostcodeInFloodArea(POSTCODE) >> {
                FloodRisk.HIGH_RISK
            }
        and: 'The correct error is thrown'
            thrown(HouseInsuranceCannotBeInsuredException)
    }

    def 'Flood area checker - medium risk'() {
        when: 'The flood risk for the area of the postcode is checked'
            def result = floodAreaCheckerAdapter.getFloodAreaRisk(POSTCODE)

        then: 'The 3rd party for a given postcode is called and indicates a medium flood risk'
            1 * floodAreaChecker.isPostcodeInFloodArea(POSTCODE) >> {
                FloodRisk.MEDIUM_RISK
            }
        and: 'The result is the expected'
            result == FLOOD_CHARGE_MEDIUM_PERCENTAGE
    }

    def 'Flood area checker - #description'() {
        when: 'The flood risk for the area of the postcode is checked'
        def result = floodAreaCheckerAdapter.getFloodAreaRisk(POSTCODE)

        then: 'The 3rd party for a given postcode is called and indicates a #description'
        1 * floodAreaChecker.isPostcodeInFloodArea(POSTCODE) >> {
            risk
        }
        and: 'The result is the expected'
            result == FLOOD_CHARGE_LOW_PERCENTAGE

        where:
        description     |       risk
        'low risk'      |       FloodRisk.LOW_RISK
        'no risk'       |       FloodRisk.NO_RISK
    }
}
