package gr.hellasdirect.houseinsurance.adapter

import gr.hellasdirect.houseinsurance.exception.HouseInsuranceCannotBeInsuredException
import gr.hellasdirect.houseinsurance.exception.HouseInsuranceException
import spock.lang.Specification
import uk.co.subsidencewatch.SubsidenceAreaChecker
import uk.co.subsidencewatch.SubsidenceAreaCheckerTechnicalFailureException

class SubsidenceAreaCheckerAdapterSpec extends Specification {
    private static final String POSTCODE = 'POSTCODE'
    private SubsidenceAreaCheckerAdapter subsidenceAreaCheckerAdapter
    private SubsidenceAreaChecker subsidenceAreaChecker

    def setup() {
        subsidenceAreaChecker = Mock()
        subsidenceAreaCheckerAdapter = new SubsidenceAreaCheckerAdapter(subsidenceAreaChecker)
    }

    def 'Check subsidence - technical fault occurs when retrieving response from 3rd party'() {
        when: 'The area of the postcode is checked if it is subsidence'
            subsidenceAreaCheckerAdapter.checkSubsidenceArea(POSTCODE)

        then: 'The 3rd party for a given postcode is called and an error is thrown'
            1 * subsidenceAreaChecker.isPostcodeInSubsidenceArea(POSTCODE) >> {
                throw new SubsidenceAreaCheckerTechnicalFailureException()
            }
        and: 'The correct error is thrown'
            thrown(HouseInsuranceException)
    }

    def 'Check postcode in subsidence area returns an exception'() {
        when: 'The area of the postcode is checked if it is subsidence'
            subsidenceAreaCheckerAdapter.checkSubsidenceArea(POSTCODE)

        then: 'The postcode is checked and is in subsidence area'
            1 * subsidenceAreaChecker.isPostcodeInSubsidenceArea(POSTCODE) >> true
        and: 'The correct error is thrown'
            thrown(HouseInsuranceCannotBeInsuredException)
    }

    def 'Check postcode not inm subsidence area does not return an exception'() {
        when: 'The area of the postcode is checked if it is subsidence'
            subsidenceAreaCheckerAdapter.checkSubsidenceArea(POSTCODE)

        then: 'The postcode is checked and is not in subsidence area'
            1 * subsidenceAreaChecker.isPostcodeInSubsidenceArea(POSTCODE) >> false
        and: 'No error is thrown'
            noExceptionThrown()
    }
}
